# Models and Manuals of DeepCob

This repo contains the trained models from the DeepCob paper (Precise and high-throughput analysis of maize cob geometry using deep learning with an application in genebank phenomics). Also, manuals with jupyter notebooks are provided for custom detections and maize cob parameter extraction on a custom dataset as well as for model updating on our pre-trained deep learning models.



## [Manual 1: tf1-manual-maskrcnn-maize-cob-detection-phenotypic-traits](./tf1-manual-maskrcnn-maize-cob-detection-phenotypic-traits)
This manual allows maize cob detection and extraction of cob parameters on either our provided datasets or on custom image data using tensorflow 1.13.1.


## [Manual 2: tf1-manual-maskrcnn-maize-model-updating](./tf2-manual-maskrcnn-maize-cob-detection-phenotypic-traits)
This manual allows maize cob detection and extraction of cob parameters on either our provided datasets or on custom image data using tensorflow 2.4.0.


## [Manual 3: tf1-manual-maskrcnn-maize-model-updating](./tf1-manual-maskrcnn-maize-model-updating)
This is a manual that allows to update our maizemodel (or any of the other models provided in ./models) on custom images using tensorflow 1.13.1. Note that the model updating does not work on tensorflow >=2.0 due to compatibility issues with our models trained on tensorflow 1.13.1.


## [models](./models)
Here we provide our resulting models from the training on datasets ImgOld and ImgNew, as well as ImgDiv and ImgCross.

[maizemodel](./models/maizemodel.h5):      the best model from training with ImgOld and ImgNew for detection
and parameter extraction on the Peruvian maize images

[imgcrossmodel-10](./models/imgcrossmodel-10.h5):        the best model after updating the maizemodel on 10 training images of ImgCross

[imgcrossmodel-20](./models/imgcrossmodel-20.h5):    the best model after updating the maizemodel on 20 training images of ImgCross

[imgcrossmodel-30](./models/imgcrossmodel-30.h5):    the best model after updating the maizemodel on 30 training images of ImgCross

[imgcrossmodel-40](./models/imgcrossmodel-40.h5):    the best model after updating the maizemodel on 40 training images of ImgCross

[imgcrossmodel-50](./models/imgcrossmodel-50.h5):   the best model after updating the maizemodel on 50 training images of ImgCross

[imgdivmodel-10](./models/imgdivmodel-10.h5):  the best model after updating the maizemodel on 10 training images of ImgDiv

[imgdivmodel-20](./models/imgdivmodel-20.h5):  the best model after updating the maizemodel on 20 training images of ImgDiv

[imgdivmodel-30](./models/imgdivmodel-30.h5):  the best model after updating the maizemodel on 30 training images of ImgDiv

[imgdivmodel-40](./models/imgdivmodel-40.h5):  the best model after updating the maizemodel on 40 training images of ImgDiv

[imgdivmodel-50](./models/imgdivmodel-50.h5):   the best model after updating the maizemodel on 50 training images of ImgDiv


## [Code for the best model M104](./code-best-model-M104)
Here we provide the code (python command line script + sh script) for running Mask R-CNN with the model parameters utilized that lead to the finally best model M104.
