# Manual to apply detection and extraction of cob parameters on any given cob image set
## Using tensorflow 1 (tested on 1.13.1)

Source of original implementation from which the mrcnn module was obtained: https://github.com/matterport/Mask_RCNN)

## 1) Set up anaconda environment
a) If not yet done: Install Anaconda
Ubuntu Version 16: https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-16-04

Ubuntu Version 18: https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart-de

Ubuntu Version 20: https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-20-04-quickstart

Windows or MacOS: Follow instructions Anaconda Website and download

b) Set up the conda environment maskrcnn-maize by extracting environment copy:
`mkdir -p maskrcnn-maize`
`tar -xzf maskrcnn-maize.tar.gz -C maskrcnn-maize`

c) Copy maskrcnn-maize into Anaconda envs folder


## 2) Activate anaconda environment
`conda activate maskrcnn-maize`

## 3) Open Jupyter notebook
a) Open jupyter (in Ubuntu type in terminal: jupyter notebook)

b) open notebook [maskrcnn-maize-cob-detection-phenotypic-traits.ipynb](./maskrcnn-maize-cob-detection-phenotypic-traits.ipynb)


## 4) Run detection and trait extraction

The only 3 things you need to adjust:

- a custom name of your dataset which will be saved in the output data:

`datasetname='test'`       

- the path to your dataset. We provide [ImgOldImgNew-validation-data](../ImgOldImgNew-validation-data) as a sample data set.
These are the images on which we compared between the three approaches in the paper. The images in this image dataset were not included in the training set for the models and therefore not biased. 

`folder='../ImgOldImgNew-validation-data'`       

- the length of the ruler element in cm

`rulerellength=0.7`       


Then run the code. Explanation of possible output text in the notebook:
- '1  von  4  images analyzed':  Counter how many images of dataset are already analyzed

- 'No ruler could be detected on image - extraction of all traits but length and diameter' : Since
no ruler could be detected, cob length and diameter cannot be calculated. If for example you have a cob dataset without ruler, but are interested in phenotypic traits like color or shape, you do not need to consider this warning message. You will find however all other output results (traits, masks,...) of this image.

- 'ruler could not be measured': The measuring of the single ruler elements was not possible, can occur occasionally when there is only part of a ruler detected, or the imaging conditions were unfavorable. In this case same as above - all results but cob length and diameter are included. 

- 'Neither cob nor ruler could be detected - image skipped' : Neither cob nor ruler was detected in this image, so it is continued with the next image without giving output of that image.

- 'No cob could be detected on image - extraction of cob traits skipped':  Since no cob but only ruler(s) were detected on the image, no results were extracted.


## 5) Understand the structure of the results


When running the code, a results folder is created with 3 subfolders:
- cobmasks: Here the cob mask images of all detected cobs are saved as .jpg images. Cobs are named after the image dataset from where they were extracted, with a count for each cob (i.e. _3).

- csvdata: Here the dataset with the extracted phenotypic traits is saved.  There are columns for the image name, dataset name, cobname (the continuous number per image), the total cobs per image, cob diameter in pixels, cob length in pixels, cob diameter in cm, cob length in cm, cob aspect ratio, cob asymmetry, cob ellipticity, cob mean red channel, cob mean green channel, cob mean blue channel. In case a ruler was not detected or could not be measured, cob diameter and length in cm are not included in the file.

- fullimg: Here you find the visualizations of the cob and ruler detections for each image on which at least one object was detected.

