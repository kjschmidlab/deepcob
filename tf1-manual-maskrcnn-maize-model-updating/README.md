# Manual to perform MaskRCNN model updating on a new maize cob image dataset
## Using tensorflow 1 (tested on 1.13.1)


Source of original implementation from which the mrcnn module was obtained: https://github.com/matterport/Mask_RCNN)


This instruction is written for application on a normal CPU (available to all researchers). We recommend using a workstation with 16 threads for fast model computation. If you want to run on a GPU, look up the modifications in the Mask R-CNN literature on the web. The conda environment we provide has CPU and GPU support. In case you want to run on a computing cluster, contact the support stuff regarding cluster-dependent requirements.


## 1) Annotate a train and validation set of your image data
We recommend annotating each 10 images for the training and validation set using
VGG Image Annotator. In each image, an accurate pixel-wise mask (polygon in via) should be drawn around each maize cob. The ruler should be annotated by two masks, one for the horizontal and one for the vertical part. A sample annotation graphic is given in the Supplementary. [Here](./instruction-annotation) we also provide a detailed annotation instruction.
Finally, the dataset folder should contain 2 subfolders:
- train: i.e. 10 images + .json file 'via_region_data'
- val: i.e. 10 images + .json file 'via_region_data'

## 2) Set up anaconda environment
a) If not yet done: Install Anaconda
Ubuntu Version 16: https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-16-04

Ubuntu Version 18: https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart-de

Ubuntu Version 20: https://www.digitalocean.com/community/tutorials/how-to-install-the-anaconda-python-distribution-on-ubuntu-20-04-quickstart

Windows or MacOS: Follow instructions Anaconda Website and download


b) Set up the conda environment maskrcnn-maize by extracting environment copy:
`mkdir -p maskrcnn-maize`
`tar -xzf maskrcnn-maize.tar.gz -C maskrcnn-maize`

c) Copy maskrcnn-maize into Anaconda envs folder

## 3) Activate anaconda environment
`conda activate maskrcnn-maize`


## 4a) Option command line script: Model updating
You can directly open a terminal and run the [command line script](./model-updating.py):
`python3 model-updating.py train --dataset=../ImgOldImgNew-validation-data --weights=../models/maizemodel --logs ./updated-models`

If you want to use your own annotated dataset, replace the name of the dataset in the command.

## 4b) Option Jupyter Notebook: Model updating
Open the jupyter notebook [model-updating.ipynb](./model-updating.ipynb) and run the command line script from the notebook.

In case you get a Kernel error, uninstall jupyter with 

`conda remove jupyter`

and install it again with

`conda install jupyter`

## 5) File location and use of updated model
After the model updating is done, you will find your updated models (one model for each epoch where the validation loss improved)
in the folder ./updated-models. If you want to save memory by saving only the model with the lowest validation loss, change
mrcnn/model.py this code line to: save_best_only=True).
You can also calculate the AP5.95 score of all updated models in case you have annotated validation data with this command:

`python3 ap.py --dataset=../ImgOldImgNew-validation-data --modelpath=./updated-models/result`

This will output a .csv file ('ap-finished.csv') containing the AP5.95 scores for all the updated models in the updated-models folder.

You can directly use the models to test the detections visually on different, not annotated images from your dataset. Please refer
for the detection to the other manuals and simply replace the model.

