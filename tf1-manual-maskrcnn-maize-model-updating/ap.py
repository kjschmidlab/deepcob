#use environment maskrcnnglob

import os
import sys
import random
import math
import re
import time
import numpy as np
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pandas as pd



# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
import mrcnn.model as modellib
from mrcnn.model import log
import cobruler
import argparse


# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

#argument parser

parser = argparse.ArgumentParser(description='Args')

parser.add_argument('--dataset', required=True,
                        help='Path to dataset')
parser.add_argument('--modelpath', required=True,
                        help='Path to models')

args = parser.parse_args()

def get_ax(rows=1, cols=1, size=16):
    """Return a Matplotlib Axes array to be used in
    all visualizations in the notebook. Provide a
    central point to control graph sizes.

    Adjust the size attribute to control how big to render images
    """
    _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    return ax


#compute mAP50
def compute_batch_ap_50(image_ids):
    APs = []
    for image_id in image_ids:
        # Load image
        image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset, config,
                                   image_id, use_mini_mask=False)
        # Run object detection
        results = model.detect([image], verbose=0)
        # Compute AP
        r = results[0]
        AP, precisions, recalls, overlaps =\
            utils.compute_ap(gt_bbox, gt_class_id, gt_mask,
                              r['rois'], r['class_ids'], r['scores'], r['masks'])
        APs.append(AP)
    return APs

#compute mAP75
def compute_batch_ap_75(image_ids):
    APs = []
    for image_id in image_ids:
        # Load image
        image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset, config,
                                   image_id, use_mini_mask=False)
        # Run object detection
        results = model.detect([image], verbose=0)
        # Compute AP
        r = results[0]
        AP, precisions, recalls, overlaps =\
            utils.compute_ap(gt_bbox, gt_class_id, gt_mask,
                              r['rois'], r['class_ids'], r['scores'], r['masks'],iou_threshold=0.75)
        APs.append(AP)
    return APs


# Compute mAP over the 50 val dataset  [.5,.95]

def compute_batch_ap_range(image_ids):
    APs = []
    for image_id in image_ids:
        # Load image
        image, image_meta, gt_class_id, gt_bbox, gt_mask =\
            modellib.load_image_gt(dataset, config,
                                   image_id, use_mini_mask=False)
        # Run object detection
        results = model.detect([image], verbose=0)
        # Compute AP
        r = results[0]
        AP=utils.compute_ap_range(gt_bbox, gt_class_id, gt_mask,
                              r['rois'], r['class_ids'], r['scores'], r['masks'],verbose=0)
        APs.append(AP)
    return APs



#make new pandas dataframe with the columns
new = pd.DataFrame({'model': [],'mAP @ IoU=.5.95': []})

MAIZE_DIR=args.dataset

config = cobruler.maizeConfig()

# Override the training configurations with a few
# changes for inferencing.
class InferenceConfig(config.__class__):
# Run detection on one image at a time
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()

# TODO: code for 'training' test mode not ready yet
TEST_MODE = "inference"

# Load validation dataset
dataset = cobruler.maizeDataset()
dataset.load_maize(MAIZE_DIR, "val")

# Must call before using the dataset
dataset.prepare()
print("Images: {}\nClasses: {}".format(len(dataset.image_ids), dataset.class_names))


modellist = [x for x in os.listdir(args.modelpath) if x.endswith(".h5")]

#specify which model
for number in modellist:
    #modelno="mask_rcnn_maize_00"+number
    MODEL_DIR= args.modelpath
    MAIZE_WEIGHTS_PATH = MODEL_DIR+"/"+number

    # Create model in inference mode

    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR,
			          config=config)

    weights_path = MAIZE_WEIGHTS_PATH
    # Load weights
    model.load_weights(weights_path, by_name=True)


    #compute mAP IoU.5.95
    APsrange = compute_batch_ap_range(dataset.image_ids)

    print(number, ": ","mAP @ IoU=.5.95: ", np.mean(APsrange) )

    new=new.append({'model': number, 'mAP @ IoU=.5.95': np.mean(APsrange)},ignore_index=True)


    new.to_csv('./ap-intermediate.csv', sep='\t')

new.to_csv('./ap-finished.csv', sep='\t')

