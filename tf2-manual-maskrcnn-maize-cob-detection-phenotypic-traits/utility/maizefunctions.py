import sys
import random
import math
import re
import time
import numpy as np
import tensorflow as tf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import skimage
from skimage.color import rgb2gray
import cv2
import pandas as pd
import time
import os



# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN implementation
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
from mrcnn import visualize
from mrcnn.visualize import display_images
import mrcnn.model as modellib
from mrcnn.model import log









#for the visualization
def get_ax(rows=1, cols=1, size=16):
    _, ax = plt.subplots(rows, cols, figsize=(size*cols, size*rows))
    return ax


#function for diameter
def get_diameter(new2,ycoords):
    test=new2[ycoords,:]
    #count at the specific y coords diameter
    unique, counts = np.unique(test, return_counts=True)
    #take only counts of true values (means how much mask at this diameter)
    if len(counts)==2:
        diameter=counts[1]
    else:
        diameter=0.000000001
    return diameter



#separate cob and ruler
def separate_classes(results): 
    cobrois=[]
    cobmasks=[]
    rulerrois=[]
    rulermasks=[]

    for t in range(0,len(results['class_ids'])):
        if results['class_ids'][t]==1:
            cobrois.append(results['rois'][t])
            cobmasks.append(results['masks'][:,:,t])

        if results['class_ids'][t]==2:  
            rulerrois.append(results['rois'][t])
            rulermasks.append(results['masks'][:,:,t])

    #format axes of mask array which is 3D        
    cobmasks=np.asarray(cobmasks)
    cobmasks=cobmasks.swapaxes(0,2)
    cobmasks=cobmasks.swapaxes(0,1)

    rulermasks=np.asarray(rulermasks)
    rulermasks=rulermasks.swapaxes(0,2)
    rulermasks=rulermasks.swapaxes(0,1)

    return cobrois, cobmasks, rulerrois, rulermasks


# Function taken from utils.dataset
def load_image(image_path):
    # Load image
    image = skimage.io.imread(image_path)
    # If grayscale. Convert to RGB for consistency.
    if image.ndim != 3:
        image = skimage.color.gray2rgb(image)
    # If has an alpha channel, remove it for consistency
    if image.shape[-1] == 4:
        image = image[..., :3]
    #return numpy array of image
    return image


def measure_ruler_threshold(cvthreshold, rulernumber, rulermasks, rulerbbox, imagegray):
  


#initialize ruler list
    listall=[]
 
    
    
    for r in range(0,rulernumber):
        

        #extract rulermask from colored image
        new=rulermasks[:,:,r]
        rulermabox=rulerbbox[r]
        imaa=imagegray*new
        new2=imaa[rulermabox[0]:rulermabox[2],rulermabox[1]:rulermabox[3]]
        new2=np.asarray(new2)


        #save temporarily ruler mask to read it in correctly by opencv
        sf=plt.figure(figsize=(rulermabox[3]-rulermabox[1],rulermabox[2]-rulermabox[0]),dpi=1.0,frameon=False)
        plt.imshow(new2,interpolation="none")
        plt.gca().set_axis_off()
        sf.subplots_adjust(bottom=0,top=1,right=1,left=0)
        plt.savefig('./utility/test.png')
        plt.close()


        #read in ruler mask by opencv
        img = cv2.imread('./utility/test.png',0)
        #ret,thresh1 = cv2.threshold(img,160,255,cv2.THRESH_BINARY)
        ret,thresh1 = cv2.threshold(img,cvthreshold,255,cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #os.remove("test.png")


        rulerelements=[]
        #find retangular contours - adjust parameters:
        for cnt in contours:
            approx = cv2.approxPolyDP(cnt, 0.1 * cv2.arcLength(cnt, True), True)
            #keep only retangular contours
            if len(approx) == 4:
                rulerelements.append(cnt)
         
        #measure the contours
        #listall=np.zeros(len(rulerelements))
        for x in range(len(rulerelements)):
            cont=(rulerelements[x][:,:,0].flatten())
            len_contour=(cont.max()-cont.min())
            listall.append(len_contour)

        #keep only contours which are in the range of the ruler elements
        #this is independent of image size for this type of ruler (1 el=2cm)!

        if img.shape[0]<img.shape[1]:
            value=img.shape[0]
        
        else:
            value=img.shape[1]
        

        
    #old  
    listall=np.asarray(listall)
    listall=listall[listall<50]
    listall=listall[listall>25]
    
    return listall


#main function detection and extraction
def detection_extraction(folder,resultpath,datasetname,rulerellength,model,dataset):




        #make a pandas dataframe for the data
    resdata = pd.DataFrame({'imgname': [],'dataset': [],'cobname': [],'totalcobimg': [],
                            'cob-diameter-pixel': [],'cob-length-pixel': [],'cob-diameter-cm': [],'cob-length-cm':[],
                            'cob-aspectratio': [], 'cob-asymmetry': [],'cob-ellipticity': [],
                            'cob-mean-red':[],'cob-mean-green':[],'cob-mean-blue':[]})




    #set counter
    counter=0


    #img names
    listofpicnames= os.listdir(folder)

    start=time.time()


    for img in os.listdir(folder):

        image = load_image(folder+'/'+img)
        imgname=listofpicnames[counter]
        print(imgname)
        counter=counter+1
        # Run object detection
        results = model.detect([image], verbose=0)

        # SAVE FULL IMAGE WITH DETECTED INSTANCES
        ax = get_ax(1)
        r = results[0]

        a = visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], 
                                    dataset.class_names, r['scores'], ax=ax,
                                    title="Predictions")

        plt.savefig(resultpath+'/fullimg/'+imgname,bbox_inches='tight', pad_inches=-0.5,orientation= 'landscape')
        plt.close()


        #continue with next image if nor cob not ruler detected
        if not 1 in r['class_ids'] and not 2 in r['class_ids'] :
            print ("Neither cob nor ruler could be detected - image skipped")
            print(counter," von ",str(len(os.listdir(folder))), " images analyzed") 
            continue


        #continue with next image if cob not detected
        elif not 1 in r['class_ids'] and 2 in r['class_ids']:
            print ("No cob could be detected on image - extraction of cob traits skipped")
            print(counter," von ",str(len(os.listdir(folder))), " images analyzed") 
            continue


        #if ruler not detected, but cob detected, extraction of all parameters but length and diameter
        elif not 2 in r['class_ids'] and 1 in r['class_ids'] :
            print ("No ruler could be detected on image - extraction of all traits but length and diameter")


            pixelpercm='NA'
            cobrois=[]
            cobmasks=[]
            for t in range(0,len(r['class_ids'])):
                if r['class_ids'][t]==1:
                    cobrois.append(r['rois'][t])
                    cobmasks.append(r['masks'][:,:,t])

            #format axes of mask array which is 3D        
            cobmasks=np.asarray(cobmasks)
            cobmasks=cobmasks.swapaxes(0,2)
            cobmasks=cobmasks.swapaxes(0,1)

            #extract cobboxes from cobmasks    
            cobbbox = utils.extract_bboxes(cobmasks)

            #extract the original boxes
            cobrect=cobrois

            #number of overall found masks of cobs in the image
            cobnumber=cobmasks.shape[2]


            for c in range(0,cobnumber):

                # EXTRACT COB WIDTH AND HEIGHT IN PIXEL FROM BOXES AND ASPECT RATIO
                cobre=cobrect[c]
                width_cobrect=cobre[3]-cobre[1]
                height_cobrect=cobre[2]-cobre[0]
                cobaspectr=width_cobrect/height_cobrect

                #COB WIDTH AND HEIGHT IN CM , adjust for ruler length in cm
                if pixelpercm!='NA':
                    cobheightcm=height_cobrect/pixelpercm*rulerellength
                    cobwidthcm=width_cobrect/pixelpercm*rulerellength


                else:
                    cobheightcm='NA'
                    cobwidthcm='NA'

                new=cobmasks[:,:,c]
                cobmabox=cobbbox[c]

                new2=new[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3]]

                ############MEAN RGB COLOR###############

                #Extract color mask

                #expand the dimensions of the binary mask to 3D
                expand=np.expand_dims(new,axis=2)
                expand2=np.concatenate((expand,expand,expand),axis=2)

                #multiply image with mask to get colored mask
                colormask=expand2*image
                #plt.imshow(colormask)

                #cut mask to its box dimensions
                colormaskcutted=colormask[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3],]

                #2)Extract the mean RGB values
                boolean=new[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3]]
                rgb=np.mean(colormaskcutted[boolean,:], axis = 0)


                # exclude a mask if <40% whole img
                cobsurfacebox=width_cobrect*height_cobrect
                unique, counts = np.unique(new2, return_counts=True)
                cobsurfacemask=counts[1]

                lambdaparam='NA'
                tparam='NA'
                asymmetry='NA'
                ellipticity='NA'

                #width an height of cutted mask
                width_cobmabox=cobmabox[3]-cobmabox[1]
                height_cobmabox=cobmabox[2]-cobmabox[0]

                # EXTRACT COB SHAPE PARAMETERS
                if cobsurfacemask >0.4*cobsurfacebox:


                    sf=plt.figure(figsize=(cobmabox[3]-cobmabox[1],cobmabox[2]-cobmabox[0]),dpi=1.0,frameon=False)
                    plt.imshow(new2,interpolation="none")
                    plt.gca().set_axis_off()
                    sf.subplots_adjust(bottom=0,top=1,right=1,left=0)
                    plt.savefig(resultpath+'/cobmasks/'+imgname+'_'+str(c+1)+'.jpg')
                    plt.close()



                    # GENERATE RADII MEASUREMENTS AFTER BAKER


                    #divide maize cob height into 8 equal parts
                    dist=int(round(height_cobmabox/8,0))

                    #generate y coordinates where diameter measurements will be taken
                    yf=dist
                    ye=dist*2
                    yd=dist*3
                    yt=dist*4
                    yc=dist*5
                    yb=dist*6
                    ya=dist*7

                    #extract mask diameter at y coordinate
                    A=get_diameter(new2,ya)
                    B=get_diameter(new2,yb)
                    C=get_diameter(new2,yc)
                    T=get_diameter(new2,yt)
                    D=get_diameter(new2,yd)
                    E=get_diameter(new2,ye)
                    F=get_diameter(new2,yf)



                    #compute shape parameters lambda,T, asymmetry, ellipticity
                    lambdaparam=10.51/((np.log(7)*np.log(7*F/A)+np.log(3)*np.log(3*E/B)+np.log(5/3)*np.log(5*D/3*C))-1)
                    tparam=np.log(3.25*A*B*C*T*D*E*F)/7
                    asymmetry=lambdaparam-1
                    ellipticity=(1/tparam)-1

                    #write results
                    resdata=resdata.append({'imgname': imgname,'dataset': datasetname,'cobname': str(c+1),
                                        'totalcobimg': cobnumber,'cob-diameter-pixel': width_cobrect,
                                        'cob-length-pixel': height_cobrect,'cob-diameter-cm':cobwidthcm ,
                                        'cob-length-cm':cobheightcm,'cob-aspectratio': cobaspectr,
                                        'cob-asymmetry': asymmetry,'cob-ellipticity': ellipticity,'cob-mean-red':rgb[0],
                                       'cob-mean-green':rgb[1],'cob-mean-blue':rgb[2]},ignore_index=True) 


        #if cob and ruler detected extract all parameters
        else: 

            #extract rois and masks for cob and ruler
            cobrois, cobmasks, rulerrois, rulermasks= separate_classes(r)    


            # EXTRACT AND SAVE RULERMASKS FOR RULER ELEMENT MEASURING
            rulerbbox = utils.extract_bboxes(rulermasks)
            rulernumber=rulermasks.shape[2]

            # EXTRACT AND SAVE COBMASKS


            #extract cobboxes from cobmasks    
            cobbbox = utils.extract_bboxes(cobmasks)


            #extract the original boxes
            cobrect=cobrois

            #number of overall found masks of cobs in the image
            cobnumber=cobmasks.shape[2]



            # RULER DETECTION
            imagegray=rgb2gray(image)

            listall=measure_ruler_threshold(160, rulernumber, rulermasks, rulerbbox, imagegray)
            if len(listall)>1 and listall.std()<7:
                #print("Mittelwert: "+str(listall.mean()) + "Median: "+str(np.median(listall))+"Standardabweichung: " + str(listall.std()))
                pixelpercm=listall.mean()
                #print("successfully measurd with 160")

            else:
                listall=measure_ruler_threshold(110, rulernumber, rulermasks, rulerbbox, imagegray) 
                if len(listall)>1 and listall.std()<7:
                    pixelpercm=listall.mean()
                    #print("successfully measurd with 110")
                else:
                    listall=measure_ruler_threshold(80, rulernumber, rulermasks, rulerbbox, imagegray) 
                    if len(listall)>1 and listall.std()<7:
                        pixelpercm=listall.mean()
                        #print("successfully measurd with 80")
                    else:
                        pixelpercm='NA'
                        #if none detected or too high standard deviation put NA to pixelpercm and cm cob measures to later measure!
                        print("ruler could not be measured")


            #loop over cob masks
            for c in range(0,cobnumber):

                # EXTRACT COB WIDTH AND HEIGHT IN PIXEL FROM BOXES AND ASPECT RATIO
                cobre=cobrect[c]
                width_cobrect=cobre[3]-cobre[1]
                height_cobrect=cobre[2]-cobre[0]
                cobaspectr=width_cobrect/height_cobrect

                #COB WIDTH AND HEIGHT IN CM , adjust for ruler length in cm
                if pixelpercm!='NA':
                    cobheightcm=height_cobrect/pixelpercm*rulerellength
                    cobwidthcm=width_cobrect/pixelpercm*rulerellength


                else:
                    cobheightcm='NA'
                    cobwidthcm='NA'

                new=cobmasks[:,:,c]
                cobmabox=cobbbox[c]

                new2=new[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3]]

                ############MEAN RGB COLOR###############

                #Extract color mask

                #expand the dimensions of the binary mask to 3D
                expand=np.expand_dims(new,axis=2)
                expand2=np.concatenate((expand,expand,expand),axis=2)

                #multiply image with mask to get colored mask
                colormask=expand2*image
                #plt.imshow(colormask)

                #cut mask to its box dimensions
                colormaskcutted=colormask[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3],]

                #2)Extract the mean RGB values
                boolean=new[cobmabox[0]:cobmabox[2],cobmabox[1]:cobmabox[3]]
                rgb=np.mean(colormaskcutted[boolean,:], axis = 0)


                # exclude a mask if <40% whole img
                cobsurfacebox=width_cobrect*height_cobrect
                unique, counts = np.unique(new2, return_counts=True)
                cobsurfacemask=counts[1]

                lambdaparam='NA'
                tparam='NA'
                asymmetry='NA'
                ellipticity='NA'

                #width an height of cutted mask
                width_cobmabox=cobmabox[3]-cobmabox[1]
                height_cobmabox=cobmabox[2]-cobmabox[0]

                # EXTRACT COB SHAPE PARAMETERS
                if cobsurfacemask >0.4*cobsurfacebox:


                    sf=plt.figure(figsize=(cobmabox[3]-cobmabox[1],cobmabox[2]-cobmabox[0]),dpi=1.0,frameon=False)
                    plt.imshow(new2,interpolation="none")
                    plt.gca().set_axis_off()
                    sf.subplots_adjust(bottom=0,top=1,right=1,left=0)
                    plt.savefig(resultpath+'/cobmasks/'+imgname+'_'+str(c+1)+'.jpg')
                    plt.close()



                    # GENERATE RADII MEASUREMENTS AFTER BAKER


                    #divide maize cob height into 8 equal parts
                    dist=int(round(height_cobmabox/8,0))

                    #generate y coordinates where diameter measurements will be taken
                    yf=dist
                    ye=dist*2
                    yd=dist*3
                    yt=dist*4
                    yc=dist*5
                    yb=dist*6
                    ya=dist*7

                    #extract mask diameter at y coordinate
                    A=get_diameter(new2,ya)
                    B=get_diameter(new2,yb)
                    C=get_diameter(new2,yc)
                    T=get_diameter(new2,yt)
                    D=get_diameter(new2,yd)
                    E=get_diameter(new2,ye)
                    F=get_diameter(new2,yf)



                    #compute shape parameters lambda,T, asymmetry, ellipticity
                    lambdaparam=10.51/((np.log(7)*np.log(7*F/A)+np.log(3)*np.log(3*E/B)+np.log(5/3)*np.log(5*D/3*C))-1)
                    tparam=np.log(3.25*A*B*C*T*D*E*F)/7
                    asymmetry=lambdaparam-1
                    ellipticity=(1/tparam)-1





                #write results
                resdata=resdata.append({'imgname': imgname,'dataset': datasetname,'cobname': str(c+1),
                                        'totalcobimg': cobnumber,'cob-diameter-pixel': width_cobrect,
                                        'cob-length-pixel': height_cobrect,'cob-diameter-cm':cobwidthcm ,
                                        'cob-length-cm':cobheightcm,'cob-aspectratio': cobaspectr,
                                        'cob-asymmetry': asymmetry,'cob-ellipticity': ellipticity,'cob-mean-red':rgb[0],
                                       'cob-mean-green':rgb[1],'cob-mean-blue':rgb[2]},ignore_index=True) 



            #resdata.to_csv(resultpath+'/csvdata/resultdata-intermediate.csv', sep='\t')


        print(counter," von ",str(len(os.listdir(folder))), " images analyzed") 


    # CSV RESULT FILE
    resdata.to_csv(resultpath+'/csvdata/resultdata.csv', sep='\t')


    end=time.time()


    print("time:"+str(round(end-start,2))+" sec")

    
